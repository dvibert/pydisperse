from setuptools import setup


setup(
    name="pydisperse",
    version=1.0,
    author="Didier Vibert  -LAM - Laboratoire d'Astrophysique de Marseille",
    author_email="didier.vibert@lam.fr",
    description=("python wrapper for DisPerSE (http://thierry-sousbie.github.io/DisPerSE/)"),
    license="GPLv3+",
    url="https://gitlab.lam.fr/dvibert/pydisperse",
    packages=['pydisperse'],
    install_requires=['numpy<2', 'matplotlib', 'scipy', 'mayavi',  # mayavi is needed for tvtk and traits
                      'unsio'],
    entry_points={
        'console_scripts': [
            'unsio_NDfield=pydisperse.unsio_NDfield:commandLine',
        ],
    },

)
