# pyDisPerSE

# create venv
```
conda create -n pydisperse "vtk" "mayavi" "libexpat<2.6" "numpy<2" scipy astropy matplotlib
```

# install from develop branch
```
git checkout origin/develop -b develop
conda activate pydisperse
cd pydisperse
pip install .
```
